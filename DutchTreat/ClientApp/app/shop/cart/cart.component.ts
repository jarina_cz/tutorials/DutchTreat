﻿import { Component } from '@angular/core';
import { DataService } from '../../shared/services/dataService';
import { Router } from '@angular/router';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})

export class CartComponent {

  constructor(private data: DataService, private router: Router) { }

  onCheckout() {
    if (this.data.loginRequired) {
      this.router.navigate(["login"])
    } else {
      this.router.navigate(["checkout"])
    }
  }
}