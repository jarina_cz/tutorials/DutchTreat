﻿import { Component } from '@angular/core';
import { DataService } from '../../shared/services/dataService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
/** checkout component*/
export class CheckoutComponent {
  errorMessage: string = '';
  /** checkout ctor */
  constructor(public data: DataService, private router: Router) { }

  onCheckout() {
    this.data.checkout()
      .subscribe(success => {
        if (success) {
          this.router.navigate(['/']);
        }
      }, err => this.errorMessage = 'Failed to save order');
  }
}