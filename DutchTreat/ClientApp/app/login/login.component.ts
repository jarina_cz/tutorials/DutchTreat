﻿import { Component } from '@angular/core';
import { DataService } from '../shared/services/dataService';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
/** login component*/
export class LoginComponent {
  errorMessage: string = '';
  public creds = {
    username: '',
    password: ''
  }
  /** login ctor */
  constructor(private data: DataService, private router: Router) {

  }

  onLogin() {
    this.data.login(this.creds)
      .subscribe(success => {
        if (success) {
          if (this.data.order.items.length == 0) {
            this.router.navigate(['/']);
          } else {
            this.router.navigate(["checkout"]);
          }
        }
      }, err => this.errorMessage = "Failed to login")
  }
}